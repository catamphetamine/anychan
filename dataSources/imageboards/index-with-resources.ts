import TwoChannel from './2ch/index-with-resources.js'
import FourChan from './4chan/index-with-resources.js'
import EightChan from './8ch/index-with-resources.js'
import EndChan from './endchan/index-with-resources.js'
import KohlChan from './kohlchan/index-with-resources.js'
import LainChan from './lainchan/index-with-resources.js'
import ArisuChan from './arisuchan/index-with-resources.js'
import SmugLoli from './smugloli/index-with-resources.js'
import LeftyPol from './leftypol/index-with-resources.js'
import TvChan from './tvchan/index-with-resources.js'
import VecchioChan from './vecchiochan/index-with-resources.js'
import Bandada from './bandada/index-with-resources.js'
import TahtaCh from './tahtach/index-with-resources.js'
import WizardChan from './wizardchan/index-with-resources.js'
import JakpartySoy from './jakparty.soy/index-with-resources.js'
import JunkuChan from './junkuchan/index-with-resources.js'
import ZzzChan from './zzzchan/index-with-resources.js'
import AlogsSpace from './alogs.space/index-with-resources.js'
import NinetyFourChan from './94chan/index-with-resources.js'
import PtChan from './ptchan/index-with-resources.js'
import DioChan from './diochan/index-with-resources.js'
import NiuChan from './niuchan/index-with-resources.js'
import TwentySevenChan from './27chan/index-with-resources.js'
import HeolCafe from './heolcafe/index-with-resources.js'

export default [
	TwoChannel,
	FourChan,
	EightChan,
	KohlChan,
	EndChan,
	LainChan,
	ArisuChan,
	TvChan,
	SmugLoli,
	LeftyPol,
	VecchioChan,
	Bandada,
	TahtaCh,
	WizardChan,
	JakpartySoy,
	JunkuChan,
	ZzzChan,
	AlogsSpace,
	NinetyFourChan,
	PtChan,
	DioChan,
	NiuChan,
	TwentySevenChan,
	HeolCafe
] as const