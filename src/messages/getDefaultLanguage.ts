import type { Locale } from "@/types";

export default function getDefaultLanguage(): Locale {
	return 'en'
}