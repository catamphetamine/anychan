export default class SubscribedThreadsUpdaterError extends Error {
	constructor(message: string) {
		super(message)
	}
}